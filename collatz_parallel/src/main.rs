#![warn(missing_docs)]

//!
//! This program test the Collatz conjeture from a given number<br>
//!
//!  -----------------------------------------------------
//!  | CPUs                    | 90000000000             |
//!  | Speed                   | 2500000 numbers/second  |
//!  |-------------------------|-------------------------|
//!  | Combinations (u128 Max) | 3.40282366920939E+038   |
//!  | Combinations / CPU      | 3.78091518801043E+027   |
//!  | Time in years/ CPU      | 47956813648026.8        |
//!  |---------------------------------------------------|
//!
//! For example try:
//! ```rust
//! cargo run 389
//! ```
//!

use rayon::prelude::*;
use std::io::{self, Write};
use std::env;
use std::thread;
use std::time::{Duration, Instant};
use std::process;

const INCREMENT: u128 = 100_000_000;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_next() {
        assert_eq!(0, get_next(0));
        assert_eq!(4, get_next(1));
        assert_eq!(1, get_next(2));
        assert_eq!(22, get_next(7));
        assert_eq!(112, get_next(37));
        assert_eq!(50, get_next(100));
        assert_eq!(2998, get_next(999));
    }

    #[test]
    fn test_collatz_sequence() {
        assert_eq!(
            collatz_sequence(9),
            [9, 28, 14, 7]
        );
        assert_eq!(
            collatz_sequence(7),
            [7, 22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5]
        );
        assert_eq!(collatz_sequence(99999).len(), 113);
        assert_eq!(
            collatz_sequence(9999999999999999999999999999999).len(),
            301
        );
    }
}

/// Given a number, obtain the next element of the Collatz sequence
///
/// Implements the Collatz formula:
///  - if number is odd then return (3 * number + 1)
///  - if number is even the return (number / 2)
///
/// # Examples
/// ```rust
/// assert_eq!(4, get_next(1));
/// assert_eq!(1, get_next(2));
/// ```
///
fn get_next(number: u128) -> u128 {
    match (number & 1) == 1 {
        true => (number << 1) + number + 1,
        false => number >> 1,
    }
}

/// Get the collatz sequence of a given number
///
///
/// # Examples
/// ```rust
/// assert_eq!(
///     collatz_sequence(9),
///     [9, 28, 14, 7, 22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1]
/// );
/// ```
///
fn collatz_sequence(number: u128) -> Vec<u128> {
    let mut current: u128 = number;
    let mut current_values: Vec<u128> = vec![number];
    while current != 1 && current >= number{
        current = get_next(current);

        if current_values.contains(&current) {
            current_values.push(current);
            println!("---> ¡¡Cycle found!!");
            println!("{:?}", current_values);
            process::exit(1);
        }
        current_values.push(current);
    }
    current_values
}

fn sleep(seconds: u32) {
    let mut wait = seconds;
    print!("Waiting");
    io::stdout().flush().unwrap();
    while wait > 0 {
        print!(".");
        io::stdout().flush().unwrap();
        wait = wait - 1;
        thread::sleep(Duration::from_secs(1));
    }
    println!();
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        println!("Usage examples: *********************");
        println!();
        println!("cargo run from # Calculates the collatz from the given number.");
        println!();
        println!("cargo run 12");
        println!();
        println!("*************************************");
        process::exit(0);
    }

    let mut from: u128 = args[1].parse().unwrap();

    loop {
        let to = from + INCREMENT;
        println!("From: {} to {}", from, to);

        let src: Vec<_> = (from..to).map(u128::from).collect();

        let start = Instant::now();

        src.into_par_iter().for_each(|item: u128| {
            collatz_sequence(item);
        });

        let duration = start.elapsed().as_secs() as u128 + 1;
        let speed = INCREMENT / duration;

        println!("Tests: {}, Durantion {} seconds,  Speed: {} numbers/second", INCREMENT, duration, speed);
        sleep(60);  // for the CPU to relax a bit.

        from = from + INCREMENT + 1;
    }
}
