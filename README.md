# Learning Rust with Collatz

Pick up a random number and apply the following simple rules:

- if the number is odd multiply it by 3 and add 1.
- if the number is even divide it by 2.

Repeat the process with the number you get as a result. At some point, you will reach the number 1. For example:

52 -> 26 -> 13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1

This is what the [Collatz conjeture](https://en.wikipedia.org/wiki/Collatz_conjecture) suggests and I used the problem as an excuse to **learn** and get a first contact with [Rust](https://doc.rust-lang.org/stable/book/).

There are two variants of the program:

- a sequencial one
- a parallel one


There are instructions on how to run each program in their respective *main.rs* files.


# Code optimization

Rust has so-called [Zero Cost Abstractions](https://medium.com/ingeniouslysimple/rust-zero-cost-abstraction-in-action-9e4e2f8bf5a) which basically means that your programming style (imperative -loops- vs functional -maps-) does not have a performance impact in the end result/executable.

I wanted to test that. In the Collatz exercises I used the "fast ways" to multiply by 3 and to divide by 2... if I had written the code in C. But this is not C, this is Rust. ¿Are the "faster" variants faster? ¿Are they slower?

The code in the "collatz_profile" folder tries to measure which method is faster. But speed depends on so many things that at the end one wants to see the generated code to verify that *Zero Cost Abstraction* thing. 

## Multiply by 3
```rust
fn mult_naive(number: u128) -> u128 {
    number * 3
}

fn mult_fast(number: u128) -> u128 {
    (number << 1) + number
}
```
The Rust compiler is smart and produces the same output for [mult_naive](https://godbolt.org/z/WEo6aGfK8) and [mult_fast](https://godbolt.org/z/1xf7PWYnb):

```asm
mov  rax, rdi
mov  ecx, 3
mul  rcx
lea  rcx, [rsi + 2*rsi]
add  rdx, rcx
ret
```

## Divide by 2

```rust

fn div_naive(number: u128) -> u128 {
    number / 2
}

fn div_fast(number: u128) -> u128 {
    number >> 1
}
```

Again, the Rust compiler is smart and produces the same output for [div_naive](
https://godbolt.org/z/Gasfa8GWq) and [div_fast](https://godbolt.org/z/a5ToszrqT):

```asm
mov  rdx, rsi
mov  rax, rdi
shrd  rax, rsi, 1
shr  rdx
ret
```


## Conclusion

You do not have to think about *micro-optimizations*, the Rust compiler is very smart and will take care for you.


BIG Thanks to [godbolt](https://godbolt.org) for the wonderful tool.
