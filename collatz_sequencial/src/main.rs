#![warn(missing_docs)]

//!
//! This program test the Collatz conjeture for a given number.<br>
//! There are two operating modes:
//! - You want to know the sequence from the given number to 1.
//! - You want to verify as many numbers as possible.
//!
//! For example try:
//! ```rust
//! cargo run sequence 99999999999999654123987512200383000389
//! cargo run calculate 1732001732
//! ```
//!

use std::collections::HashSet;
use std::convert::TryFrom;
use std::env;
use std::process;

const MAX_CACHE_SIZE: u128 = 400_000_000;
const REPORT_COUNT: u32 = 1_000_000;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_next() {
        assert_eq!(0, get_next(0));
        assert_eq!(4, get_next(1));
        assert_eq!(1, get_next(2));
        assert_eq!(22, get_next(7));
        assert_eq!(112, get_next(37));
        assert_eq!(50, get_next(100));
        assert_eq!(2998, get_next(999));
    }

    #[test]
    fn test_collatz_sequence() {
        assert_eq!(
            collatz_sequence(9),
            [9, 28, 14, 7, 22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1]
        );
        assert_eq!(
            collatz_sequence(7),
            [7, 22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1]
        );
        assert_eq!(collatz_sequence(99999).len(), 227);
        assert_eq!(
            collatz_sequence(9999999999999999999999999999999).len(),
            1040
        );
    }
}

/// Given a number, obtain the next element of the Collatz sequence
///
/// Implements the Collatz formula:
///  - if number is odd then return (3 * number + 1)
///  - if number is even the return (number / 2)
///
/// # Examples
/// ```rust
/// assert_eq!(4, get_next(1));
/// assert_eq!(1, get_next(2));
/// ```
///
fn get_next(number: u128) -> u128 {
    match (number & 1) == 1 {
        true => (number << 1) + number + 1,
        false => number >> 1,
    }
}

/// Get the collatz sequence of a given number
///
///
/// # Examples
/// ```rust
/// assert_eq!(
///     collatz_sequence(9),
///     [9, 28, 14, 7, 22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1]
/// );
/// ```
///
fn collatz_sequence(number: u128) -> Vec<u128> {
    let mut current: u128 = number;
    let mut current_values: Vec<u128> = vec![number];
    while current != 1 {
        current = get_next(current);

        print!("{}, ", current);
        if current_values.contains(&current) {
            current_values.push(current);
            println!("---> ¡¡Cycle found!!");
            println!("{:?}", current_values);
            process::exit(1);
        }
        current_values.push(current);
    }
    current_values
}

/// Helper function which will clear the cache
///
/// This function will delete from the cache all the numbers smaller than the
/// current number being tested. We know that all the numbers under the current
/// one are compliant (if we tested them before, of course).
///
/// This function exists to avoid taking all the memory from the system it is
/// running on. The size of the cache is controlled by the contant `MAX_CACHE_SIZE`.
///
/// # Examples
/// ```rust
/// clean_compliant(&mut compliant_numbers, current_number, &mut cache_positives);
/// ```
///
fn clean_compliant(compliant: &mut HashSet<u128>, number: u128, cache_positives: &mut u128) {
    let before: u128 = u128::try_from(compliant.len()).unwrap();
    compliant.retain(|&k| k > number);
    let max: u128 = *compliant.iter().max().unwrap();
    let positives = *cache_positives;

    if before > MAX_CACHE_SIZE {
        // The cache is too big, lets start again.
        compliant.clear();
        *cache_positives = 0;
    }

    let after: u128 = u128::try_from(compliant.len()).unwrap();
    print_info(number, before, after, max, positives);
}

/// Print some statistics on the terminal
///
/// Useful to know that the program is doing something.
///
fn print_info(number: u128, before: u128, after: u128, max: u128, cache_positives: u128) {
    println!(
        "Numbers < {} comply. Cache size before: {} and after: {}. Max: {}. Cache positives: {}.",
        number, before, after, max, cache_positives
    );
}

/// Optimized version of `collatz_sequence` which uses a cache to calculate faster
/// if a given number complies with the Collatz conjeture. Its aim is to be as fast
/// as possible, because it _of course_ should test **all** the numbers in the `u128` space.
///
fn fast_collatz(number: u128, compliant: &mut HashSet<u128>, cache_positives: &mut u128) {
    let mut current: u128 = number;
    let mut current_values: Vec<u128> = vec![number];
    while current != 1 {
        current = get_next(current);
        match current < number || compliant.contains(&current) {
            true => {
                *cache_positives += 1;
                break; // Number already compliant.
            }
            false => compliant.insert(current),
        };

        if current_values.contains(&current) {
            current_values.push(current);
            println!("---> ¡¡Cycle found!!");
            println!("{:?}", current_values);
            process::exit(1);
        }
        current_values.push(current);
    }
}

/// This verifies from the given number
///
/// TODO: until when??? find u128MAX.
///
///
fn start_fast_calculation(number: u128) {
    let mut cache_positives: u128 = 0;
    let mut compliant_numbers: HashSet<u128> = HashSet::new();
    let mut time_to_clean = REPORT_COUNT;

    let mut to_test: u128 = number;

    loop {
        if !compliant_numbers.contains(&to_test) {
            fast_collatz(to_test, &mut compliant_numbers, &mut cache_positives);
        }

        match time_to_clean == 0 {
            true => {
                time_to_clean = REPORT_COUNT; // Reset it to original value.
                clean_compliant(&mut compliant_numbers, to_test, &mut cache_positives);
            }
            false => time_to_clean -= 1,
        }

        to_test += 1;
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 3 {
        println!("Usage examples: *********************");
        println!("cargo run sequence 345 # Returns the collatz sequence of 345.");
        println!();
        println!("cargo run calculate 345 # Applies the conjeture from 345 onwards.");
        println!();
        println!("*************************************");
        process::exit(0);
    }

    if args[1] == "sequence" && args.len() == 3 {
        let result = collatz_sequence(args[2].parse().unwrap());
        println!("Collatz sequence: {:?}", result);
        println!("Sequence length: {:?}", result.len());
    }

    if args[1] == "calculate" && args.len() == 3 {
        start_fast_calculation(args[2].parse().unwrap());
    }
}

/*****
 Value is 216_000_432. We  have 469_346_309 compliant numbers
 Numbers <    38_000_040 comply. Cache size before:  45564354 and after:  44564353. Max:     306296925203752
 Numbers <   642_000_643 comply. Cache size before:  17938948 and after:  16938947. Max: 1414236446719942480. Cache positives: 3999852.
 Numbers < 1_366_001_366 comply. Cache size before:  13718310 and after:  12718309. Max:    2121948424744000. Cache positives: 2999997.
 Numbers < 1_732_001_732 comply. Cache size before: 127754948 and after: 126754947. Max: 28206742763994640. Cache positives: 29964618.
*****/
