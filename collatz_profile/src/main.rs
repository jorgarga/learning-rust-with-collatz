use std::cmp::Ordering;
use std::thread::sleep;
use std::time::{Duration, Instant};

fn mult_naive(number: u128) -> u128 {
    number * 3
}

fn mult_fast(number: u128) -> u128 {
    (number << 1) + number
}

fn div_naive(number: u128) -> u128 {
    number / 2
}

fn div_fast(number: u128) -> u128 {
    number >> 1
}

fn measure(func: impl Fn(u128) -> u128) -> Option<Duration> {
    let start = Instant::now();

    for number in 1..100_000_000 {
        func(number);
    }

    return Instant::now().checked_duration_since(start);
}

fn compare(name1: &str, op1: impl Fn(u128) -> u128, name2: &str, op2: impl Fn(u128) -> u128) {
    let t1 = measure(op1);
    let t2 = measure(op2);
    match t1.cmp(&t2) {
        Ordering::Less => {
            println!(
                "Less {} is faster than {} by {} ns. t1={:?}, t2={:?}",
                name1, name2, (t2.unwrap().checked_sub(t1.unwrap())).unwrap().as_nanos(), t1, t2
            )
        }
        Ordering::Greater => {
            println!(
                "Greater {} is faster than {} by {} ns. t1={:?}, t2={:?}",
                name2, name1, (t1.unwrap().checked_sub(t2.unwrap())).unwrap().as_nanos(), t2, t1
            )
        }
        Ordering::Equal => {
            println!("{} and {} are equal.", name2, name1)
        }
    }
}

fn main() {
    println!("Start");

    for _ in 0..4 {
        compare("Naive mult", mult_naive, "Fast mult", mult_fast);
        compare("Naive div", div_naive, "Fast div", div_fast);

        sleep(Duration::from_secs(3));
        println!("---");

        compare("Fast mult", mult_fast, "Naive mult", mult_naive);
        compare("Fast div", div_fast, "Naive div", div_naive);

        sleep(Duration::from_secs(3));
        println!("==========");
    }
}
